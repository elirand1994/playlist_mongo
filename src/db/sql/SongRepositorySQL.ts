import { OkPacket, RowDataPacket } from "mysql2";
import ResourceNotFoundException from "../../exceptions/SqlExceptions/ResourceNotFound.exception.js"
import { Artist,Song, ResponseObj } from "../../interfaces/interface.types";
import {connection as db} from "./sql.connection.js";
import artistRepositorySQL from "../sql/ArtistRepositorySQL.js";
import ResourceNotFound from "../../exceptions/SqlExceptions/ResourceNotFound.exception.js";
class SongRepostorySQL {

    create_song = async (payload : Song) =>{
        await artistRepositorySQL.get_artist_by_id(payload.artist_id.toString());
        const sql = 'INSERT INTO songs SET ?';
        const results = await db.query(sql,payload);
        const result : RowDataPacket = results[0] as RowDataPacket;
        const song = await this.get_song_by_id(result.insertId);
        return song;
    };   
    get_all_songs = async ()  =>{
        const sql = 'SELECT * FROM songs';
        const results = await db.query(sql);
        const result : RowDataPacket = results[0] as RowDataPacket;
        return result;
    }
    get_song_by_id = async (id : string) =>{
        const sql = `SELECT * FROM songs WHERE song_id = ${id}`;
        const results = await db.query(sql);
        const result : RowDataPacket = results[0] as RowDataPacket;
        if (result.length === 0) throw new ResourceNotFoundException(`Song with id ${id} was not found`);
        return result[0];
    }

    delete_all_songs_by_artist_id = async (id : string) =>{
        await artistRepositorySQL.get_artist_by_id(id);
        const sql = `DELETE FROM songs WHERE artist_id = ${id}`;
        const results = await db.query(sql);
        const result : RowDataPacket = results[0] as RowDataPacket;
        return result;
    }
    delete_all_songs = async ()  =>{
        const sql = "DELETE FROM songs";
        const results = await db.query(sql);
        const result : OkPacket = results[0] as OkPacket;
        return result;
        
        // delete all from playlist_song
    }

    get_all_songs_from_artist_by_id = async (id : string) =>{
        await artistRepositorySQL.get_artist_by_id(id);
        const sql = `SELECT * FROM songs WHERE artist_id = ${id}`;
        const results = await db.query(sql);
        const result : RowDataPacket = results[0] as RowDataPacket;
        return result[0];
    }

    delete_song_by_id = async (id : string) =>{
        const sql = `DELETE FROM songs WHERE song_id = ?`;
        const results = await db.query(sql,id);
        const result : RowDataPacket = results[0] as RowDataPacket;
        if (result.affectedRows === 0) throw new ResourceNotFound(`Song with id ${id} has not been found`);
        return result;
    }


    update_song_by_id = async (id: string, body: Song) =>{
        await this.get_song_by_id(id);
        const sql = "UPDATE songs SET ? WHERE song_id = ?";
        const results = await db.query(sql,[body,id]);
        const result : RowDataPacket = results[0] as RowDataPacket;
        return result;
    }
}

const songsRepositorySQL = new SongRepostorySQL();
export default songsRepositorySQL;
