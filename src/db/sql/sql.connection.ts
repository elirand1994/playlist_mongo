import mysql from "mysql2/promise";
import {env} from "../../utils/env.js";
import log from "@ajar/marker";
export let connection : mysql.Connection;

export const connect = async () =>{
    if (connection) return connection;
    connection = await mysql.createConnection({
    host : env.SQL_HOST,
    port : Number(env.SQL_PORT),
    database : env.SQL_DB_NAME,
    user : env.SQL_USER,
    password : env.SQL_PASSWORD
    });
    await connection.connect();
    log.magenta("Connected to MySQL DB");
}