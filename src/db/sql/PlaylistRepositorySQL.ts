import { OkPacket, RowDataPacket } from "mysql2";
import ResourceNotFound from "../../exceptions/SqlExceptions/ResourceNotFound.exception.js";
import { Playlist, Playlist_Song } from "../../interfaces/interface.types.js";
import songsRepositorySQL from "./SongRepositorySQL.js";
import {connection as db} from "./sql.connection.js";
import userRepositorySQL from "./UserRepositorySQL.js";
class ArtistsRepositorySQL {

    create_playlist = async (payload : Playlist) =>{
        // check if user with id exists, if not, throw error:
        await userRepositorySQL.get_user_by_id(payload.user_id.toString());
        // if it passes, user exists, continue adding the playlist to db
        const sql = 'INSERT INTO playlists SET ?';
        const results = await db.query(sql,payload);
        const result : RowDataPacket = results[0] as RowDataPacket;
        const playlist = await this.get_playlist_by_id(result.insertId);
        return playlist;
    };  
    
    get_all_playlists = async () =>{
        const sql = 'SELECT * FROM playlists';
        const results = await db.query(sql);
        const result : RowDataPacket = results[0] as RowDataPacket;
        return result;
    }

    get_playlist_by_id = async (id : string) =>{
        const sql = `SELECT * FROM playlists WHERE playlist_id = ?`;
        const results = await db.query(sql,id);
        const result : RowDataPacket[] = results[0] as RowDataPacket[];
        if (result.length === 0) throw new ResourceNotFound(`Playlist with id ${id} not found!`);
        return result[0];
    }

    delete_playlist_by_id = async (id : string) =>{
        const playlist = await this.get_playlist_by_id(id);
        const sql = `DELETE FROM playlists WHERE playlist_id = ?`;
        const results = await db.query(sql,id);
        return playlist
    }

    update_playlist_by_id = async (id: string, body: Playlist) =>{
        await this.get_playlist_by_id(id);
        const sql = "UPDATE playlists SET ? WHERE playlist_id = ?";
        const results = await db.query(sql,[body,id]);
        const result : OkPacket = results[0] as OkPacket;
        return result;
    }

    delete_all_playlists = async () =>{
        const sql = "DELETE FROM playlists";
        const results = await db.query(sql);
        const result : OkPacket = results[0] as OkPacket;
        return result;
    }
    
    add_song_to_playlist = async (playlistid: string, songid : string) =>{
        console.log(playlistid,songid)
        await songsRepositorySQL.get_song_by_id(songid);
        await this.get_playlist_by_id(playlistid);
        const playlist_song : Playlist_Song = {
            playlist_id : Number(playlistid),
            song_id : Number(songid)
        }
        const sql = "INSERT INTO playlist_song SET ?"
        const results = await db.query(sql,playlist_song);
        const result : OkPacket = results[0] as OkPacket; 
        return result;
    }

}

const artistsRepositorySQL = new ArtistsRepositorySQL();
export default artistsRepositorySQL;
