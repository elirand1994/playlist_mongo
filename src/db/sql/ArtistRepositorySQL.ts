import { OkPacket, RowDataPacket } from "mysql2";
import ResourceNotFound from "../../exceptions/SqlExceptions/ResourceNotFound.exception.js";
import { Artist, ResponseObj } from "../../interfaces/interface.types.js";
import {connection as db} from "./sql.connection.js";
class ArtistsRepositorySQL {

    create_artist = async (payload : Artist) =>{
        const sql = 'INSERT INTO artists SET ?';
        const results = await db.query(sql,payload);
        const result : RowDataPacket = results[0] as RowDataPacket;
        const artist_packet = await this.get_artist_by_id(result.insertId);
        return artist_packet;
    };  
    
    get_all_artists = async () =>{
        const sql = 'SELECT * FROM artists';
        const results = await db.query(sql);
        const result : RowDataPacket = results[0] as RowDataPacket;
        return result;
    }

    get_artist_by_id = async (id : string) =>{
        const sql = `SELECT * FROM artists WHERE artist_id = ?`;
        const results = await db.query(sql,id);
        const result : RowDataPacket[] = results[0] as RowDataPacket[];
        if (result.length === 0) throw new ResourceNotFound(`Artist with id ${id} not found!`);
        console.log(result);
        return result[0];
    }

    delete_artist_by_id = async (id : string) =>{
        await this.get_artist_by_id(id);
        const sql = `DELETE FROM artists WHERE artist_id = ?`;
        const results = await db.query(sql,id);
        const result : RowDataPacket = results[0] as RowDataPacket;
        return result[0];
    }

    delete_all_artists = async () =>{
        const sql = "DELETE FROM artists";
        const results = await db.query(sql);
        const result : OkPacket = results[0] as OkPacket;
        return result;
    }

    update_artist_by_id = async (id: string, body: Artist) =>{
        await this.get_artist_by_id(id);
        const sql = "UPDATE artists SET ? WHERE artist_id = ?";
        const results = await db.query(sql,[body,id]);
        const result : OkPacket = results[0] as OkPacket;
        return result;
    }

}

const artistsRepositorySQL = new ArtistsRepositorySQL();
export default artistsRepositorySQL;
