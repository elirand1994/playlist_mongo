import { OkPacket, RowDataPacket } from "mysql2";
import ResourceNotFound from "../../exceptions/SqlExceptions/ResourceNotFound.exception.js";
import {User } from "../../interfaces/interface.types.js";
import {connection as db} from "./sql.connection.js";
import * as UTILS from "../../utils/helpers.utils.js"
import bcrypt from 'bcrypt';
class UserRepositorySQL {

    create_user = async (payload : User) =>{
        const sql = 'INSERT INTO users SET ?';
        const salt = await bcrypt.genSalt(10);
        const hashed_password = await bcrypt.hash(payload.password, salt);
        payload.password = hashed_password;
        const results = await db.query(sql,payload);
        const result : RowDataPacket = results[0] as RowDataPacket;
        console.log(result);
        const user = await this.get_user_by_id(result.insertId);
        const {access_token,refresh_token} = await UTILS.createTokens(payload);
        user.refresh_token = refresh_token;
        await this.update_user_by_id(user.user_id.toString(),user as User);
        delete user.refresh_token;
        return {user,refresh_token,access_token};
    };  
    
    get_all_users = async () =>{
        const sql = 'SELECT * FROM users';
        const results = await db.query(sql);
        const result : RowDataPacket = results[0] as RowDataPacket;
        return result;
    }

    get_user_by_id = async (id : string) =>{
        const sql = `SELECT * FROM users WHERE user_id = ?`;
        const results = await db.query(sql,id);
        const result : RowDataPacket[] = results[0] as RowDataPacket[];
        if (result.length === 0) throw new ResourceNotFound(`User with id ${id} not found!`);
        return result[0];
    }

    delete_user_by_id = async (id : string) =>{
        await this.get_user_by_id(id);
        const sql = `DELETE FROM users WHERE user_id = ?`;
        const results = await db.query(sql,id);
        const result : RowDataPacket = results[0] as RowDataPacket;
        return result[0];
    }

    update_user_by_id = async (id: string, body: User) =>{
        await this.get_user_by_id(id);
        const sql = "UPDATE users SET ? WHERE user_id = ?";
        const results = await db.query(sql,[body,id]);
        const result : OkPacket = results[0] as OkPacket;
        return result;
    }

    login_user = async (email : string , password : string) =>{
        const user = await this.get_user_by_email(email);
        const bool = await bcrypt.compare(password,user.password);
        if (!bool) throw new ResourceNotFound(`Wrong password for user with email ${email}`);
        const {access_token,refresh_token} = await UTILS.createTokens(user);
        user.refresh_token = refresh_token;
        await this.update_user_by_id(user.user_id,user as User);
        return {access_token,refresh_token};
    }

    get_user_by_email = async (email : string) => {
        const sql = "SELECT * FROM users WHERE user_id = ?";
        const results = await db.query(sql,email);
        const result : RowDataPacket[] = results[0] as RowDataPacket[];
        if (result.length === 0) throw new ResourceNotFound(`User with email ${email} not found!`);
        return result[0];

    }

}

const userRepositorySQL = new UserRepositorySQL();
export default userRepositorySQL;


// delete_all_artists = async () =>{
    //     const sql = "DELETE FROM artists";
    //     const results = await db.query(sql);
    //     const result : OkPacket = results[0] as OkPacket;
    //     return result;
    // }