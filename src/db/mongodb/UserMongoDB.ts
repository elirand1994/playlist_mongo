import { ErrorResponse, ResponseObj } from "../../interfaces/interface.types.js";
import user_model from "../../modules/User/user.model.js";
import * as UTILS from "../../utils/constants.utils.js";
import mongoose from "mongoose";
import {createTokens} from "../../utils/helpers.utils.js";
import bcrypt from 'bcrypt';

class UserMongoService {
    createUserMongoService = async (body:any) =>{
        const salt = await bcrypt.genSalt(10);
        const hashed_password = await bcrypt.hash(body.password, salt);
        body.password = hashed_password;
        const user = await user_model.create(body);
        const {access_token,refresh_token} = await createTokens(user);
        user.token = refresh_token;
        user.save();
        const response: ResponseObj = {
            code: 200,
            message: `User with id ${user._id} has been created!`,
            data: user,
            refresh_token : refresh_token,
            access_token : access_token
        };
        return response;
    }

    loginMongoService = async(body : any) =>{
        const user = await user_model.findOne({email : body.email});
        const bool = await bcrypt.compare(body.password,user.password);
        if (!bool){
            const errorRes : ErrorResponse = {
                code : 500,
                message : 'wrong password!',
                data: body.password
            }
            return errorRes;
        }
        const {access_token,refresh_token} = await createTokens(user);
        user.token = refresh_token;
        user.save();
        const response: ResponseObj = {
            code: 200,
            message: `User with id ${user._id} has been logged in!`,
            data: user,
            refresh_token : refresh_token,
            access_token : access_token
        };
        return response;
    }

    logoutMongoService = async (id : string) =>{
        const user = await user_model.findById(id);
        user.token = "";
        await user.save();
        console.log(user);
        const response : ResponseObj = {
            code : 200,
            message : `User ${id} has been logged out!`
        };
        return response;
    };

    paginateMongoService = async (page: number, to_limit: number) => {
        const to_skip = page * UTILS.USERS_PER_PAGE;
        const result = await user_model
            .find()
            .skip(to_skip)
            .limit(to_limit as number);
        const response: ResponseObj = {
            code: 200,
            message: `${to_limit} users from page ${page} have been loaded!`,
            data: result,
        };
        return response;
    };

    getAllUsersMongoSerivce = async () => {
        const result = await user_model.find();
        const response: ResponseObj = {
            code: 200,
            message: "All users have been loaded.",
            data: result,
        };
        return response;
    };
    getUserByIdMongoService = async (id: string) => {
        const given_id = mongoose.Types.ObjectId.isValid(id);
        if (!given_id) {
            const errResponse: ErrorResponse = {
                code: 403,
                message: `User with id ${id} has not been found!`,
            };
            return errResponse;
        }
        const user = await user_model.findById(id);
        if (!user) {
            const errResponse: ErrorResponse = {
                code: 403,
                message: `User with id ${id} has not been found!`,
            };
            return errResponse;
        } else {
            const response: ResponseObj = {
                code: 200,
                message: `User with id ${id} has been loaded`,
                data: user,
            };
            return response;
        }
    };

    deleteUserByIdMongoService = async (id: string) => {
        const given_id = mongoose.Types.ObjectId.isValid(id);
        if (!given_id) {
            const errResponse: ErrorResponse = {
                code: 403,
                message: `User with id ${id} has not been found!`,
            };
            return errResponse;
        } else {
            const user = await user_model.findByIdAndDelete(id);
            if (user) {
                const response: ResponseObj = {
                    code: 200,
                    message: `User with id ${given_id} has been deleted.`,
                };
                return response;
            } else {
                const response: ResponseObj = {
                    code: 403,
                    message: `User with id ${given_id} has not been found!.`,
                };
                return response;
            }
        }
    };

    updateUserByIdMongoService = async (id: string, body: any) => {
        const given_id = mongoose.Types.ObjectId.isValid(id);
        if (!given_id) {
            const errResponse: ErrorResponse = {
                code: 403,
                message: `User with id ${id} has not been found!`,
            };
            return errResponse;
        }
        const user = await user_model.findByIdAndUpdate(id, body);
        if (!user) {
            const errResponse: ErrorResponse = {
                code: 403,
                message: `User with id ${id} has not been found!`,
            };
            return errResponse;
        } else {
            const response: ResponseObj = {
                code: 200,
                message: `User with id ${given_id} has been deleted.`,
                data: user,
            };
            return response;
        }
    };

}

const userMongoService = new UserMongoService();
export default userMongoService;