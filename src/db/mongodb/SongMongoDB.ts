import song_model from "../../modules/Song/song.model.js";
import artist_model from "../../modules/Artist/artist.model.js";
import playlist_model from "../../modules/Playlist/playlist.model.js";
import { ResponseObj, Song } from "../../interfaces/interface.types.js";
class SongServiceMongo {
    addSong = async (body: Song) => {
        const artist = await artist_model.findById(body.artist);
        const song = await song_model.create(body);
        artist.songs.push(song);
        await artist.save();
        const response: ResponseObj = {
            code: 200,
            message: `Song with id ${song._id} has been created!`,
            data: song,
        };
        return response;
    };

    getSongById = async (id: string) => {
        const song = await song_model.findById(id);
        const response: ResponseObj = {
            code: 200,
            message: "Song has been loaded",
            data: song,
        };
        return response;
    };

    getAllSongs = async () => {
        const songs = await song_model.find();
        const msg =
            songs.length > 0
                ? `Found ${songs.length} songs`
                : "No songs found!";
        const response: ResponseObj = {
            code: 200,
            message: msg,
            data: songs,
        };
        return response;
    };

    updateSongById = async (id: string, body: Song) => {
        await song_model.findByIdAndUpdate(id, body);
        const response: ResponseObj = {
            code: 200,
            message: `Song ${id} has been updated.`,
        };
        return response;
    };

    deleteSongById = async (id: string) => {
        const song = await song_model
            .findById(id)
            .populate("playlists")
            .populate("artist");
        const playlists = song.playlists;
        for (const playlist of playlists) {
            const pulled_playlist = await playlist_model.findById(
                playlist._id.toString()
            );
            await pulled_playlist.songs.pull(id);
            await pulled_playlist.save();
        }
        const artist = await artist_model.findById(song.artist._id.toString());
        await artist.songs.pull(id);
        await artist.save();
        await song_model.findByIdAndDelete(id);
        const response: ResponseObj = {
            code: 200,
            message: `Song ${id} has been deleted.`,
        };
        return response;
    };

    deleteAllSongs = async () => {
        const all_songs = await song_model.find();
        for (const song of all_songs) {
            await this.deleteSongById(song._id.toString());
        }
        const response: ResponseObj = {
            code: 200,
            message: "All songs have been deleted!",
        };
        return response;
    };
}

const songServiceMongo = new SongServiceMongo();
export default songServiceMongo;
