import artist_model from "../../modules/Artist/artist.model.js";
import playlist_model from "../../modules/Playlist/playlist.model.js";
import song_model from "../../modules/Song/song.model.js";
import {
    ResponseObj,
    Artist,
    Playlist,
} from "../../interfaces/interface.types.js";
class PlaylistServiceMongo {
    getAllPlaylists = async () => {
        const playlists = await playlist_model.find();
        const response: ResponseObj = {
            code: 200,
            message: "All playlists have been loaded.",
            data: playlists,
        };
        return response;
    };

    getPlaylistById = async (id: string) => {
        const playlist = await playlist_model.findById(id);
        const response: ResponseObj = {
            code: 200,
            message: `Playlist ${playlist.title} has been loaded.`,
            data: playlist,
        };
        return response;
    };

    createPlaylist = async (body: Playlist) => {
        const playlist = await playlist_model.create(body);
        const response: ResponseObj = {
            code: 200,
            message: `Playlist ${playlist.title} has been created.`,
            data: playlist,
        };
        return response;
    };

    deletePlaylistById = async (id: string) => {
        const playlist = await playlist_model.findById(id);
        for (const song of playlist.songs) {
            const pulled_song = await song_model.findById(song._id.toString());
            await pulled_song.playlists.pull(id);
            await pulled_song.save();
        }
        await playlist_model.findByIdAndDelete(id);
        const response: ResponseObj = {
            code: 200,
            message: `Playlist ${playlist.title} has been deleted.`,
            data: playlist,
        };
        return response;
    };

    deleteAllPlaylists = async () => {
        const all_playlists = await playlist_model.find(); // get all playlists
        for (const playlist of all_playlists) {
            await this.deletePlaylistById(playlist._id.toString());
        }
        await playlist_model.deleteMany(); // deletes all playlists

        const response: ResponseObj = {
            code: 200,
            message: "All playlists have been deleted",
        };
        return response;
    };

    addSongToPlaylistById = async (id: string, songid: string) => {
        const playlist = await playlist_model.findById(id);
        const song = await song_model.findById(songid);
        playlist.songs.push(song);
        song.playlists.push(playlist);
        await playlist.save();
        await song.save();
        const response: ResponseObj = {
            code: 200,
            message: `Song ${song.title} has been added to the playlist ${playlist.title}`,
            data: playlist,
        };
        return response;
    };

    getAllSongsFromPlaylistById = async (id: string) => {
        const playlist = await playlist_model.findById(id);
        const response: ResponseObj = {
            code: 200,
            message: `All songs for playlist ${playlist.title} have been loaded.`,
            data: playlist.songs,
        };
        return response;
    };

    getAllArtistsFromPlaylistById = async (id: string) => {
        const playlist = await playlist_model.findById(id);
        const artists: Artist[] = [];
        const seen_artists = new Set<string>();
        for (const song of playlist.songs) {
            const artist = await artist_model.findById(song.artist.toString());
            if (!seen_artists.has(artist._id.toString())) {
                artists.push(artist);
                seen_artists.add(artist._id.toString());
            }
        }
        const response: ResponseObj = {
            code: 200,
            message: `All artists for playlist ${playlist.title} have been loaded.`,
            data: artists,
        };
        return response;
    };

    deleteSongFromPlaylistById = async (id: string, songid: string) => {
        const playlist = await playlist_model.findById(id);
        const song = await song_model.findById(songid);
        await playlist.songs.pull(songid);
        await song.playlists.pull(id);
        await playlist.save();
        await song.save();
        const response: ResponseObj = {
            code: 200,
            message: `Song ${song.title} has been removed from playlist ${playlist.title}`,
            data: playlist,
        };
        return response;
    };
}

const playlistServiceMongo = new PlaylistServiceMongo();
export default playlistServiceMongo;
