import artist_model from "../../modules/Artist/artist.model.js";
import song_model from "../../modules/Song/song.model.js";
import playlist_model from "../../modules/Playlist/playlist.model.js";

import { ResponseObj, Artist } from "../../interfaces/interface.types.js";
class ArtistServiceMongo {
    createArtist = async (body: Artist) => {
        const artist = await artist_model.create(body);
        const response: ResponseObj = {
            code: 200,
            message: `Artist with id ${artist._id} has been created!`,
            data: artist,
        };
        return response;
    };

    getArtistById = async (id: string) => {
        const artist = await artist_model.findById(id).populate("songs");
        const response: ResponseObj = {
            code: 200,
            message: `User with id ${id} has been loaded`,
            data: artist,
        };
        return response;
    };

    getAllArtists = async () => {
        const result = await artist_model.find().populate("songs");
        const response: ResponseObj = {
            code: 200,
            message: "All artists have been loaded.",
            data: result,
        };
        return response;
    };

    deleteArtistById = async (id: string) => {
        const artist = await artist_model.findById(id);
        for (const song of artist.songs) {
            const pulled_song = await song_model.findById(song.toString());
            const playlists = pulled_song.playlists;
            for (const playlist of playlists) {
                const pulled_playlist = await playlist_model.findById(
                    playlist.toString()
                );
                await pulled_playlist.songs.pull(song._id.toString());
                await pulled_playlist.save();
            }
            await song_model.findByIdAndDelete(song.toString());
        }
        await artist_model.findByIdAndDelete(id);
        const response: ResponseObj = {
            code: 200,
            message: `Artist with id ${id} has been deleted`,
        };
        return response;
    };

    deleteAllArtists = async () => {
        const all_artists = await artist_model.find();
        console.log(all_artists);
        for (const artist of all_artists) {
            await this.deleteArtistById(artist._id.toString());
        }
        const response: ResponseObj = {
            code: 200,
            message: "All artists have been deleted.",
        };
        return response;
    };

    updateArtistById = async (id: string, body: Artist) => {
        const artist = await artist_model.findByIdAndUpdate(id, body);
        const response: ResponseObj = {
            code: 200,
            message: `Artist ${id} has been updated.`,
            data: artist,
        };
        return response;
    };

    getAllSongsFromArtist = async (id: string) => {
        const artist = await artist_model.findById(id).populate("songs");
        const response: ResponseObj = {
            code: 200,
            message: `All the songs for artist : ${id} have been loaded.`,
            data: artist.songs,
        };
        return response;
    };
}

const artistServiceMongo = new ArtistServiceMongo();
export default artistServiceMongo;
