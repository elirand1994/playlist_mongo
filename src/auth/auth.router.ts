import raw from "../middleware/InitMiddlewares/route.async.wrapper.js";
import express from "express";
import user_controller from "../controllers/SqlControllers/user.controller.js";
import { SchemaValidation } from "../middleware/ValidationMiddlewares/verify_user_scheme.middleware.js";
import authMiddleware from "../middleware/AuthMiddlewares/auth.middleware.js";

const router = express.Router();
router.use(express.json());

router.get("/get_access_token",raw(authMiddleware.getAccessToken));
router.post("/register",raw(user_controller.register));
// router.post("/login",raw(user_controller.login));
// router.post("/logout",raw(authMiddleware.verifyAuth),raw(user_controller.logout));

export default router;
