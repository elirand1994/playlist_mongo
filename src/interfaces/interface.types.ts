
export interface GeneralResponse {
    [key: string]: any;
}
export interface ResponseObj extends GeneralResponse {
    code: number;
    message: string;
    data?: any;
    refresh_token?:any,
    access_token?:any
}

export interface ErrorResponse extends GeneralResponse {
    code: number;
    message: string;
    stack?: string;
}

export interface User {
    first_name : string;
    last_name : string;
    email : string;
    phone : string;
    password : string;
    refresh_token : string;
    roles: string;
}

export interface Song {
    name: string;
    artist_id: number;
    status_id : number;
}
export interface Artist {
    first_name: string;
    last_name: string;
    status_id : number;
}

export interface Playlist {
    title: string;
    user_id : number; 
}

export interface Playlist_Song {
    playlist_id : number;
    song_id : number;
}
