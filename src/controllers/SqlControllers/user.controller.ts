import { NextFunction, Request, Response } from "express";
import userServiceSQL from "../../service/SqlService/user.sql.service.js";
import ms from 'ms'
import { ResponseObj } from "../../interfaces/interface.types.js";
class UserController {

    getAllUsers = async (req: Request, res: Response) => {
        const users = await userServiceSQL.getAllUsers();
        const response : ResponseObj = {
            code : 200,
            message : users.length > 0 ? "All users have been loaded" : "No users found!",
            data : users
        }
        res.status(response.code).json(response);
    };

    getUserById = async (req: Request, res: Response) => {
        const user = await userServiceSQL.getUsertById(req.params.id);
        const response : ResponseObj = {
            code : 200,
            message : `User with id ${req.params.id} has been loaded`,
            data : user
        }
        res.status(response.code).json(response);
    };

    deleteUserById = async (req: Request, res: Response) => {
        const deleted_user_res = await userServiceSQL.deleteUserById(req.params.id);
        const response : ResponseObj = {
            code : 200,
            message : `User with id ${req.params.id} has been deleted`,
            data : deleted_user_res
        }
        res.status(response.code).json(response);
    };

    updateUserById = async (req: Request, res: Response) => {
        const user = await userServiceSQL.updateUserById(req.params.id,req.body);
        const response : ResponseObj = {
            code : 200,
            message : `User with id ${req.params.id} has been updated!`,
            data : user 
        }
        res.status(response.code).json(response);
    };

    register = async (req: Request, res: Response) => {
        const {user,refresh_token,access_token} = await userServiceSQL.createUser(req.body);
        res.cookie('refresh_token',refresh_token, {
            maxAge: ms('60d'), //60 days
            httpOnly: true
        })
        res.setHeader("x-access-token",access_token);
        const response : ResponseObj = {
            code : 200,
            message : `User with id ${user.user_id} has been created!`,
            data: user
        }
        res.status(response.code).json(response);
    };

    login = async (req: Request, res: Response) => {
        const {access_token,refresh_token} = await userServiceSQL.login(req.body.email,req.body.password);
        const response : ResponseObj = {
            code : 200,
            message : `User has been logged in!`
        }
        res.cookie('refresh_token',refresh_token, {
            maxAge: ms('60d'), //60 days
            httpOnly: true
          })
        res.setHeader("x-access-token",access_token);
        res.status(response.code).json(response);

    };

    // logout = async (req : Request , res : Response, next : NextFunction) =>{
    //     console.log(req.user_id);
    //     const response = await userServiceSQL.logoutService(req.user_id);
    //     res.setHeader("x-access-token","");
    //     res.clearCookie("refresh_token");
    //     res.status(response.code).json(response);
    // }
}

const user_controller = new UserController();
export default user_controller;




    // createUser = async (req: Request, res: Response) => {
    //     const response = await userServiceSQL.createUser(req.body);
    //     res.status(response.code).json(response);
    // };