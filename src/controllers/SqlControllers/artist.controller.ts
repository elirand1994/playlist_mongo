import { Request, Response } from "express";
import { ResponseObj } from "../../interfaces/interface.types.js";
import artistServiceSQL from "../../service/SqlService/artist.sql.service.js";
class ArtistController {
    getAllArtists = async (req: Request, res: Response) => {
        const artists = await artistServiceSQL.getAllArtists();
        const response : ResponseObj ={
            code : 200,
            message : artists.length > 0 ? "All artists have been loaded!" : "No artists found!",
            data : artists
        }
        res.status(response.code).json(response);
    };

    getArtistById = async (req: Request, res: Response) => {
        const artist = await artistServiceSQL.getArtistById(req.params.id);
        const response : ResponseObj ={
            code : 200,
            message : `Artist with id ${req.params.id} has been loaded`,
            data : artist
        }
        res.status(response.code).json(response);
    };

    createArtist = async (req: Request, res: Response) => {
        const artist = await artistServiceSQL.createArtist(req.body);
        const response : ResponseObj ={
            code : 200,
            message : `Artist with id :${artist.artist_id}  has been created!`,
            data : artist
        }
        res.status(response.code).json(response);
    };

    updateArtistById = async (req: Request, res: Response) => {
        const artist = await artistServiceSQL.updateArtistById(
            req.params.id,
            req.body
        );
        const response : ResponseObj ={
            code : 200,
            message : `Artist with id :${req.params.id}  has been updated!`,
            data : artist
        }
        res.status(response.code).json(response);
    };

    deleteArtistById = async (req: Request, res: Response) => {
        const deleted_artist_res = await artistServiceSQL.deleteArtistById(req.params.id);
        const response : ResponseObj ={
            code : 200,
            message : `Artist with id ${req.params.id} has been deleted`,
            data : deleted_artist_res
        }
        res.status(response.code).json(response);
    };

    deleteAllArtists = async (req: Request, res: Response) => {
        const deleted_artists_res = await artistServiceSQL.deleteAllArtists();
        const response : ResponseObj ={
            code : 200,
            message : deleted_artists_res.affectedRows > 0 ? "All artists deleted"
            : "No artists deleted!",
            data : `${deleted_artists_res.affectedRows} have been deleted`
        }
        res.status(response.code).json(response);
    };
}

const artistController = new ArtistController();
export default artistController;
