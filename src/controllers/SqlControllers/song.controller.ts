import { Request, Response } from "express";
import { ResponseObj } from "../../interfaces/interface.types.js";
import songServiceSQL from "../../service/SqlService/songs.sql.service.js";

class SongController {
    getAllSongs = async (req: Request, res: Response) => {
        const songs = await songServiceSQL.getAllSongs();
        const response : ResponseObj ={
            code : 200,
            message : songs.length > 0 ? "All songs loaded" : "No songs found!",
            data : songs
        }
        res.status(response.code).json(response);
    };

    getSongById = async (req: Request, res: Response) => {
        const song = await songServiceSQL.getSongById(req.params.id);
        const response : ResponseObj ={
            code : 200,
            message : `Song with id :${song.song_id}  has been loaded`,
            data : song
        }
        res.status(response.code).json(response);
    };

    updateSongById = async (req: Request, res: Response) => {
        const song = await songServiceSQL.updateSongById(req.params.id,req.body);
        const response : ResponseObj ={
            code : 200,
            message : `Song with id :${req.params.id}  has been updated`,
            data : `${song.affectedRows} rows have been updated`
        }
        res.status(response.code).json(response);
    };

    deleteAllSongsByArtistId = async (req: Request, res: Response) => {
        const deleted_songs_res = await songServiceSQL.deleteAllSongsByArtistId(req.params.id);
        const response : ResponseObj ={
            code : 200,
            message : deleted_songs_res.affectedRows > 0 ? 
            `All songs have been deleted for artist ${req.params.id}`
            : `No songs found for artist ${req.params.id}`,
            data : `${deleted_songs_res.affectedRows} songs deleted`
        }
        res.status(response.code).json(response);
    };

    createSong = async (req: Request, res: Response) => {
        const song = await songServiceSQL.createSong(req.body);
        const response : ResponseObj ={
            code : 200,
            message : `Song with id :${song.song_id}  has been created!`,
            data : song
        }
        res.status(response.code).json(response);
    };

    deleteAllSongs = async (req: Request, res: Response) => {
        const deleted_songs_res = await songServiceSQL.deleteAllSongs();
        const response : ResponseObj ={
            code : 200,
            message : deleted_songs_res.affectedRows > 0 ?
            "All songs deleted" : "No songs have been deleted",
            data : `${deleted_songs_res.affectedRows} have been deleted`
        }
        res.status(response.code).json(response);
    };

    getAllSongsOfArtistById  = async (req: Request, res: Response) => {
        const songs = await songServiceSQL.getAllSongsFromArtistById(req.params.id);
        const response : ResponseObj ={
            code : 200,
            message : songs.length > 0 ?
            `All songs for artist ${req.params.id} have been loaded` :
            `No songs found for artist ${req.params.id}`,
            data : songs
        }
        res.status(response.code).json(response);
    };

    deleteSongById = async (req: Request, res : Response) =>{
        const deleted_song_res = await songServiceSQL.deleteSongById(req.params.id);
        const response : ResponseObj ={
            code : 200,
            message : `Song ${req.params.id} has been deleted`,
            data : deleted_song_res
        }
        res.status(response.code).json(response);
    }
}

const songController = new SongController();
export default songController;
