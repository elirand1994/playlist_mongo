import { Request, Response } from "express";
import { ResponseObj } from "../../interfaces/interface.types.js";
import PlaylistServiceSQL from "../../service/SqlService/playlist.sql.service.js";
class PlaylistController {

    getAllPlaylists = async (req: Request, res: Response) => {
        const playlists = await PlaylistServiceSQL.getAllPlaylists();
        const response : ResponseObj = {
            code : 200,
            message : "All playlists have been loaded",
            data : playlists
        }
        res.status(response.code).json(response);
    };

    getPlaylistById = async (req: Request, res: Response) => {
        const playlist = await PlaylistServiceSQL.getPlaylistById(req.params.id);
        const response : ResponseObj = {
            code : 200,
            message : `Playlist ${playlist.playlist_id} has been loaded`,
            data : playlist
        }
        res.status(response.code).json(response);
    };

    deleteSongFromPlaylistById = async (req: Request, res: Response) => {
        const response = await PlaylistServiceSQL.deleteSongFromPlaylistById(
            req.params.id,
            req.params.songid
        );
        res.status(response.code).json(response);
    };

    deletePlaylistById = async (req: Request, res: Response) => {
        const deleted_playlist = await PlaylistServiceSQL.deletePlaylistById(
            req.params.id
        );
        const response : ResponseObj = {
            code : 200,
            message : `Playlist with id ${req.params.id} has been deleted`,
            data : deleted_playlist
        };
        res.status(response.code).json(response);
    };

    deleteAllPlaylists = async (req: Request, res: Response) => {
        const deleted_playlists = await PlaylistServiceSQL.deleteAllPlaylists();
        const response : ResponseObj = {
            code : 200,
            message : "All playlists have been deleted",
            data : deleted_playlists
        }
        res.status(response.code).json(response);
    };

    createPlaylist = async (req: Request, res: Response) => {
        const playlist = await PlaylistServiceSQL.createPlaylist(req.body);
        const response : ResponseObj = {
            code : 200,
            message : `Playlist ${playlist.playlist_id} has been created`,
            data : playlist
        }
        res.status(response.code).json(response);
    };

    updatePlaylistById = async (req : Request, res : Response) =>{
        const playlist = await PlaylistServiceSQL.updatePlaylistById(req.params.id,req.body);
        const response : ResponseObj = {
            code : 200,
            message : `Playlist ${req.params.id} has been updated`,
            data : playlist
        }
        res.status(response.code).json(response);
    }

    addSongToPlaylist = async (req : Request, res : Response) =>{
        const playlist_song = await PlaylistServiceSQL.addSongToPlaylist(req.params.playlistid,req.params.songid);
        const response : ResponseObj = {
            code : 200,
            message : `Song ${req.params.song_id} has been added to ${req.params.playlist_id}`,
            data : playlist_song
        }
        res.status(response.code).json(response);
    }
}

const playlistController = new PlaylistController();
export default playlistController;
