import { NextFunction, Request, Response } from "express";
import userService from "../service/MongoService/user.service.js";
import ms from 'ms'
import cookieParser from 'cookie-parser'

class AuthController {

    register = async (req: Request, res: Response) => {
        const response = await userService.createUserService(req.body);
        res.cookie('refresh_token',response.refresh_token, {
            maxAge: ms('60d'), //60 days
            httpOnly: true
        })
        res.setHeader("x-access-token",response.access_token);
        delete response.refresh_token;
        delete response.access_token;
        res.status(response.code).json(response);
    };
    
    login = async (req: Request, res: Response) => {
        const response = await userService.loginService(req.body);
        res.cookie('refresh_token',response.refresh_token, {
            maxAge: ms('60d'), //60 days
            httpOnly: true
          })
        res.setHeader("x-access-token",response.access_token);
        res.status(response.code).json(response);
    };
    
    logout = async (req : Request , res : Response, next : NextFunction) =>{
        console.log(req.user_id);
        const response = await userService.logoutService(req.user_id);
        res.setHeader("x-access-token","");
        res.clearCookie("refresh_token");
        res.status(response.code).json(response);
    }

}

const authController = new AuthController();
export default authController;

