import raw from "../../middleware/InitMiddlewares/route.async.wrapper.js";
import express from "express";
import artistController from "../../controllers/SqlControllers/artist.controller.js";
import { verify_id_artists } from "../../middleware/ValidationMiddlewares/verify_id_artist.middleware.js";
const router = express.Router();
router.use(express.json());

router.get("/", raw(artistController.getAllArtists)); //DONE
router.get("/:id", raw(artistController.getArtistById)); //DONE
router.get("/songs/:id", raw(artistController.getAllSongsFromArtist)); //DONE
router.put("/:id",raw(artistController.updateArtistById)); //DONE
router.delete("/:id",raw(artistController.deleteArtistById)); //DONE
router.delete("/", raw(artistController.deleteAllArtists));
router.post("/", raw(artistController.createArtist)); //DONE
export default router;
