import raw from "../../middleware/InitMiddlewares/route.async.wrapper.js";
import express from "express";
import songController from "../../controllers/SqlControllers/song.controller.js";
import { verify_id_songs } from "../../middleware/ValidationMiddlewares/verify_id_songs.middleware.js";
const router = express.Router();
router.use(express.json());

router.get("/", raw(songController.getAllSongs)); //DONE
router.get("/:id",raw(songController.getSongById)); //DONE
router.put("/:id", raw(songController.updateSongById)); //DONE
router.delete("/:id", raw(songController.deleteSongById)); //DONE
router.post("/", raw(songController.createSong)); //DONE
router.delete("/", raw(songController.deleteAllSongs)); //DONE
router.delete("/artist/:id", raw(songController.deleteAllSongsByArtistId)); //DONE
router.get("/artist/:id", raw(songController.getAllSongsOfArtistById)); //DONE

export default router;
