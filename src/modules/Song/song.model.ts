import mongoose from "mongoose";
const { Schema, model } = mongoose;

export const SongSchema = new Schema(
    {
        title: { type: String, required: true },
        artist: { type: Schema.Types.ObjectId, required: true, ref: "artist" },
        playlists: [
            { type: Schema.Types.ObjectId, required: false, ref: "playlist" },
        ],
    },
    { timestamps: true }
);

export default model("song", SongSchema);
