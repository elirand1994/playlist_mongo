import raw from "../../middleware/InitMiddlewares/route.async.wrapper.js";
import express from "express";
import playlistController from "../../controllers/SqlControllers/playlist.controller.js";
import { verify_id_playlist } from "../../middleware/ValidationMiddlewares/verify_id_playlist.middle.js";
const router = express.Router();
router.use(express.json());

router.get("/", raw(playlistController.getAllPlaylists)); // DONE
router.get("/:id",raw(playlistController.getPlaylistById)); // DONE
router.put("/:playlistid/:songid/",raw(playlistController.addSongToPlaylist)); // DONE
router.put("/:id/",raw(playlistController.updatePlaylistById)); // DONE
router.delete("/:id/:songid",raw(playlistController.deleteSongFromPlaylistById)); //Done
router.delete("/:id",raw(playlistController.deletePlaylistById)); // DONE
router.delete("/", raw(playlistController.deleteAllPlaylists)); //DONE
router.post("/", raw(playlistController.createPlaylist)); // DONE
export default router;
