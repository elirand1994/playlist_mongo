import mongoose from "mongoose";
const { Schema, model } = mongoose;
import { SongSchema } from "../Song/song.model.js";
export const PlaylistSchema = new Schema(
    {
        title: { type: String, required: true },
        songs: [SongSchema],
    },
    { timestamps: true }
);

export default model("playlist", PlaylistSchema);
