import mongoose from "mongoose";
import {PlaylistSchema} from '../Playlist/playlist.model.js';
const { Schema, model } = mongoose;

const UserSchema = new Schema({
    first_name: String,
    last_name: String,
    email: String,
    phone: String,
    playlists : [PlaylistSchema],
    password : String,
    refresh_token : String
});

export default model("user", UserSchema);
