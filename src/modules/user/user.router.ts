import raw from "../../middleware/InitMiddlewares/route.async.wrapper.js";
import express from "express";
import controller from "../../controllers/SqlControllers/user.controller.js";
import { SchemaValidation } from "../../middleware/ValidationMiddlewares/verify_user_scheme.middleware.js";
import authMiddleware from "../../middleware/AuthMiddlewares/auth.middleware.js";

const router = express.Router();
router.use(express.json());

router.get("/", raw(controller.getAllUsers));
router.get("/:id",raw(controller.getUserById));
router.put("/:id", raw(SchemaValidation), raw(controller.updateUserById));
router.delete("/:id", raw(controller.deleteUserById));
//router.post("/",raw(SchemaValidation), raw(controller.createUser));

export default router;
