import Joi from "joi";

export const CreateUser = Joi.object({
    first_name: Joi.string().alphanum().min(0).max(30).required(),
    last_name: Joi.string().alphanum().min(0).max(30).required(),

    email: Joi.string()
        .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
        .required(),

    phone: Joi.string().length(10).regex(/^\d+$/),
});

export const UpdateUser = Joi.object({
    first_name: Joi.string().alphanum().min(0).max(30),
    last_name: Joi.string().alphanum().min(0).max(30),
    email: Joi.string().email({
        minDomainSegments: 2,
        tlds: { allow: ["com", "net"] },
    }),

    phone: Joi.string().length(10).regex(/^\d+$/),
});
