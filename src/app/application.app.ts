import express from "express";
import cors from "cors";
import morgan from "morgan";
import { connect_db } from "../db/mongodb/mongoose.connection.js";
import {connect} from "../db/sql/sql.connection.js"
import user_router from "../modules/User/user.router.js";
import artist_router from "../modules/Artist/artist.router.js";
import song_router from "../modules/Song/song.router.js";
import playlist_router from "../modules/Playlist/playlist.router.js";
import initMiddlewares from "../middleware/InitMiddlewares/initializers.middlewares.js";
import errorMiddlewares from "../middleware/ErrorMiddlewares/errors.middlewares.js";
import authRouter from "../auth/auth.router.js";
import { env } from "../utils/env.js";
import cookie from "cookie-parser";
import log from "@ajar/marker";

class App {
    private app: express.Application;

    constructor() {
        // Setting up the app
        this.app = express();
        this.initializer();
    }

    private initializer = (): void => {
        this.app.use(express.json());
        this.app.use(express.urlencoded({ extended: true }));
        this.app.use(morgan("dev"));
        this.app.use(cors());
        this.app.use(cookie());
        
        this.app.use(initMiddlewares.ReqLogMiddleware);
        this.app.use(initMiddlewares.generateId);

        this.app.use("/auth",authRouter)
        this.app.use("/api/users", user_router);
        this.app.use("/api/artists", artist_router);
        this.app.use("/api/songs", song_router);
        this.app.use("/api/playlists", playlist_router);

        this.app.use(errorMiddlewares.NotFoundMiddlewware);
        this.app.use(errorMiddlewares.WriteToErrorLogFileMiddleware());
        this.app.use(errorMiddlewares.ErrorResponseMiddleWare);
    };

    public start = async (): Promise<void> => {
        await connect();
        await this.app.listen(env.PORT, env.SQL_HOST);
        log.magenta(
            "api is live on",
            ` ✨ ⚡  http://${env.HOST}:${env.PORT} ✨ ⚡`
        );
    };
}

const app = new App();
export default app;
