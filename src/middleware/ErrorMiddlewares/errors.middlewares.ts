import fs from "fs";
import log from "@ajar/marker";
import { Request, Response, NextFunction } from "express";
import HttpException from "../../exceptions/Http.exceptions";
import ResourceNotFoundException from "../../exceptions/ResourceNotFound.exceptions.js";
import { ErrorResponse } from "../../interfaces/interface.types";
const { White, Reset, Red } = log.constants;
import { ERROR_LOG_PATH } from "../../utils/constants.utils.js";
class ErrorMiddlewares {
    NotFoundMiddlewware = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        log.info(`url: ${White}${req.url}${Reset}${Red} not found...`);
        next(new ResourceNotFoundException(req.url));
    };

    WriteToErrorLogFileMiddleware = () => {
        console.log("in write");
        const errorsFileLogger = fs.createWriteStream(ERROR_LOG_PATH, {
            flags: "a",
        });
        return (
            error: HttpException,
            req: Request,
            res: Response,
            next: NextFunction
        ) => {
            errorsFileLogger.write(
                `${req.id} :: ${error.status} :: ${error.message} >> ${error.stack} \n`
            );
            next(error);
        };
    };

    ErrorResponseMiddleWare = async (
        err: HttpException,
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        const response: ErrorResponse = {
            code: err.status || 500,
            message: err.message,
            stack: err.stack,
        };
        res.status(response.code).json(response);
    };
}

const errorMiddlewares = new ErrorMiddlewares();
export default errorMiddlewares;
