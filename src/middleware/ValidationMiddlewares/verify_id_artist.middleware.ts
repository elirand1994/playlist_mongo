import { Request, Response, NextFunction } from "express";
import artist_model from "../../modules/Artist/artist.model.js";
import {
    GET_METHOD,
    DELETE_METHOD,
    PUT_METHOD,
} from "../../utils/constants.utils.js";
import ResourceNotFoundException from "../../exceptions/ResourceNotFound.exceptions.js";
import mongoose from "mongoose";
import IdNotValidException from "../../exceptions/IdNotValidException.exceptions.js";
export const verify_id_artists = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        let casting: boolean;
        if (req.method === GET_METHOD || req.method === PUT_METHOD) {
            casting = mongoose.Types.ObjectId.isValid(req.params.id);
            if (!casting) {
                next(new IdNotValidException(req.params.id));
                return;
            }
            const artist = await artist_model.findById(req.params.id);
            if (!artist) {
                next(new ResourceNotFoundException(req.params.id));
                return;
            }
        } else if (req.method === DELETE_METHOD && req.params.id) {
            const casting = mongoose.Types.ObjectId.isValid(req.params.id);
            if (!casting) {
                next(new IdNotValidException(req.params.id));
                return;
            }
            const artist = await artist_model.findById(req.params.id);
            if (!artist) {
                next(new ResourceNotFoundException(req.params.id));
                return;
            }
        }
        next();
    } catch (err) {
        next(err);
    }
};
