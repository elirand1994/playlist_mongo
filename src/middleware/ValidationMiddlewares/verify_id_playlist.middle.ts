import { Request, Response, NextFunction } from "express";
import song_model from "../../modules/Song/song.model.js";
import playlist_model from "../../modules/Playlist/playlist.model.js";

import {
    GET_METHOD,
    DELETE_METHOD,
    PUT_METHOD,
} from "../../utils/constants.utils.js";
import mongoose from "mongoose";
import IdNotValidException from "../../exceptions/IdNotValidException.exceptions.js";
import ResourceNotFoundException from "../../exceptions/ResourceNotFound.exceptions.js";
export const verify_id_playlist = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        let casting: boolean;
        if (req.method === GET_METHOD) {
            if (req.params.id) {
                casting = mongoose.Types.ObjectId.isValid(req.params.id);
                if (!casting) {
                    next(new IdNotValidException(req.params.id));
                    return;
                }
                const playlist = await playlist_model.findById(req.params.id);
                if (!playlist) {
                    next(new ResourceNotFoundException(req.params.id));
                    return;
                } else {
                    next();
                    return;
                }
            }
        } else if (req.method === PUT_METHOD) {
            casting = mongoose.Types.ObjectId.isValid(req.params.id);
            const song_id_casting = mongoose.Types.ObjectId.isValid(
                req.params.songid
            );
            if (!casting) {
                next(new IdNotValidException(req.params.id));
                return;
            }
            if (!song_id_casting) {
                next(new IdNotValidException(req.params.songid));
                return;
            }
            const playlist = await playlist_model.findById(req.params.id);
            if (!playlist) {
                next(new ResourceNotFoundException(req.params.id));
                return;
            }
            const song = await song_model.findById(req.params.songid);
            if (!song) {
                next(new ResourceNotFoundException(req.params.songid));
                return;
            }
        } else if (req.method === DELETE_METHOD) {
            if (req.params.id && req.params.songid) {
                const castingErr = "";
                casting = mongoose.Types.ObjectId.isValid(req.params.id);
                const song_id_casting = mongoose.Types.ObjectId.isValid(
                    req.params.songid
                );
                if (!casting) {
                    castingErr.concat(req.params.id);
                }
                if (!song_id_casting) {
                    castingErr.concat(" ").concat(req.params.songid);
                }
                if (castingErr.length > 0) {
                    next(new IdNotValidException(castingErr));
                }
                const playlist = await playlist_model.findById(req.params.id);
                const song = await song_model.findById(req.params.songid);
                const errString = "";
                if (!playlist) errString.concat(req.params.id);
                if (!song) errString.concat(" ").concat(req.params.songid);
                if (errString.length > 0) {
                    next(new ResourceNotFoundException(errString));
                    return;
                }
            }
            if (req.params.id) {
                casting = mongoose.Types.ObjectId.isValid(req.params.id);
                if (!casting) {
                    next(new IdNotValidException(req.params.id));
                }
                const playlist = await playlist_model.findById(req.params.id);
                if (!playlist) {
                    next(new ResourceNotFoundException(req.params.id));
                    return;
                }
            }
        }
        next();
    } catch (err) {
        next(err);
    }
};
