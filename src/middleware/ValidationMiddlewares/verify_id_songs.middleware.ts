import { Request, Response, NextFunction } from "express";
import song_model from "../../modules/Song/song.model.js";
import artist_model from "../../modules/Artist/artist.model.js";
import { POST_METHOD } from "../../utils/constants.utils.js";
import ResourceNotFoundException from "../../exceptions/ResourceNotFound.exceptions.js";
import mongoose from "mongoose";
import IdNotValidException from "../../exceptions/IdNotValidException.exceptions.js";
export const verify_id_songs = async (
    req: Request,
    res: Response,
    next: NextFunction
) => {
    try {
        let resource: string;
        let casting: boolean;
        if (req.method !== POST_METHOD) {
            casting = mongoose.Types.ObjectId.isValid(req.params.id);
            if (!casting) {
                next(new IdNotValidException(req.params.id));
                return;
            }
            const song = await song_model.findById(req.params.id);
            if (!song) {
                next(new ResourceNotFoundException(req.params.id));
                return;
            } else {
                next();
                return;
            }
        } else {
            //wants to post a new song, so see if artist exists
            resource = req.body.artist;
            casting = mongoose.Types.ObjectId.isValid(resource);
            if (!casting) {
                next(new IdNotValidException(resource));
                return;
            }
            const artist = await artist_model.findById(resource);
            if (!artist) {
                next(new ResourceNotFoundException(resource));
                return;
            } else {
                next();
                return;
            }
        }
    } catch (err) {
        next(err);
    }
};
