import { NextFunction, Request, Response } from "express";
import {env} from "../../utils/env.js";
import jwt from "jsonwebtoken";
import {ResponseObj } from "../../interfaces/interface.types.js";
import user_model from "../../modules/User/user.model.js";
import ValidationException from "../../exceptions/ValidationException.exceptions.js";
class AuthMiddleware {
    verifyAuth = async (req:Request, res : Response, next : NextFunction) => {
        try {     
            // check header or url parameters or post parameters for token
            const access_token = req.headers['x-access-token'] as string;
      
            if (!access_token) {
                next(new ValidationException("No token provided."))
            }
      
            // verifies secret and checks exp
            const decoded = await jwt.verify(access_token, env.APP_SECRET)
      
            // if everything is good, save to request for use in other routes
            req.user_id = decoded.id;
            next();
      
        } catch (error) {
            next(new ValidationException("Failed to authenticate token, access token expired."));
        }
      }

    getAccessToken = async (req : Request, res : Response, next : NextFunction) =>{
        const {refresh_token} = req.cookies;
        if (!refresh_token) {
            next(new ValidationException("No refresh token found."))
        }
      
        try{
          // verifies secret and checks expiration
          const decoded = await jwt.verify(refresh_token, env.APP_SECRET)
          
          //check user refresh token in DB
          const {id} = decoded;
          const user = await user_model.findById(id);
          if (user.refresh_token === refresh_token){
            const access_token = jwt.sign({ id , some:'other value'}, env.APP_SECRET, {
                expiresIn: env.ACCESS_TOKEN_EXPIRATION //expires in 1 minute
            })
            const response : ResponseObj = {
                code : 200,
                message : "Access token loaded",
                access_token : access_token
            }
            res.setHeader("x-access-token",access_token);
            res.status(response.code).json(response);
          } else {
              next(new ValidationException("Access token invalid!"));
          }
        }catch(err){
            console.log('error: ',err)
            next(new ValidationException("Failed to verify refresh_token."));
        }   
      }
}

const authMiddleware = new AuthMiddleware();
export default authMiddleware;