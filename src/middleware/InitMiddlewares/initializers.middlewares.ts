import fs from "fs/promises";
import { NextFunction, Request, Response } from "express";
import { checkIfExists } from "../../utils/helpers.utils.js";
import { REQ_LOG_PATH } from "../../utils/constants.utils.js";
import {generateReqId} from "../../utils/helpers.utils.js";
import log from "@ajar/marker";

class InitializerMiddlewares {
    ReqLogMiddleware = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        log.cyan("Writing to the log...");
        const flag = await checkIfExists(REQ_LOG_PATH);
        if (!flag) {
            console.log("Creating log file...");
            await fs.writeFile(REQ_LOG_PATH, JSON.stringify([]), "utf-8");
        }
        const content =
            req.method + " " + req.originalUrl + " " + Date.now() + "" + "\r\n";
        await fs.appendFile(REQ_LOG_PATH, content);
        log.green("Completed writing to log!");
        next();
    };

    generateId = async (
        req: Request,
        res: Response,
        next : NextFunction
    ) => {
        log.cyan("Generating id for req...");
        req.id = generateReqId();
        log.green(`Completed generating id...${req.id}`);
        next();
    }
}

const initMiddlewares = new InitializerMiddlewares();
export default initMiddlewares;
