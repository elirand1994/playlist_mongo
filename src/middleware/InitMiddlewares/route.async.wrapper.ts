import { Request, Response, NextFunction, RequestHandler } from "express";

export default function raw(cb: RequestHandler) {
    return async function (req: Request, res: Response, next: NextFunction) {
        try {
            await cb(req, res, next);
        } catch (err) {
            next(err);
        }
    };
}
