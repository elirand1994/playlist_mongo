import fs from "fs/promises";
import { env } from "../utils/env.js";
import jwt from 'jsonwebtoken'

export const generateReqId = () => {
    return Math.random().toString(36).slice(2);
};

export const checkIfExists = async (path: string) => {
    try {
        await fs.access(path);
        return true;
    } catch (err) {
        return false;
    }
};

export const createTokens = async(user : any) =>{
    const access_token = jwt.sign({ id : user.user_id , some:'other value'}, env.APP_SECRET, {
        expiresIn: env.ACCESS_TOKEN_EXPIRATION // expires in 1 minute
      })
      
      const refresh_token = jwt.sign({ id : user.user_id , profile:JSON.stringify(user)}, env.APP_SECRET, {
        expiresIn: env.REFRESH_TOKEN_EXPIRATION // expires in 60 days... long-term... 
      })
      console.log(refresh_token.length)
      return {access_token,refresh_token}
}
