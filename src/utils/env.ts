import { cleanEnv, str, num } from "envalid";

export const env = cleanEnv(process.env, {
    NODE_ENV: str({
        choices: ["development", "test", "production", "staging"],
    }),
    HOST: str({ default: "localhost" }),
    PORT: num({ default: 8080 }),
    //MONGO RELATED

    DB_URL: str({ default: "mongodb://localhost:27017/PlaylistDB" }),
    CLIENT_ORIGIN : str({default :"http://localhost:3000"}),

    //AUTHENTICATION RELATED
    APP_SECRET:str({default : "supercalifragilisticexpialidocious"}),
    ACCESS_TOKEN_EXPIRATION : str({default : "10m"}),
    REFRESH_TOKEN_EXPIRATION:str({default :"60d"}),

    //SQL RELATED
    SQL_HOST : str ({default : "localhost"}),
    SQL_PORT : num ({default : 3306}),
    SQL_DB_NAME : str ({default : "playlist"}),
    SQL_USER : str ({default : "root"}),
    SQL_PASSWORD : str ({default :"qwerty"})

});

