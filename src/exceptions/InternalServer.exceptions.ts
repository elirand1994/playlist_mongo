import HttpException from "./Http.exceptions.js";
class InternalServerException extends HttpException {
    constructor(path: string) {
        super(
            500,
            `Something went wrong...\nThe request : ${path} could not be complete`
        );
    }
}

export default InternalServerException;
