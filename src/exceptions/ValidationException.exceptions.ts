import HttpException from "./Http.exceptions.js";

class ValidationException extends HttpException {
    constructor(path: string) {
        super(403, `Validation error! ${path}`);
    }
}

export default ValidationException;
