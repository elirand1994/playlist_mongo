import HttpException from "../Http.exceptions.js";

class ResourceNotFound extends HttpException {
    constructor(data : string) {
        super(404, data);
    }
}

export default ResourceNotFound;
