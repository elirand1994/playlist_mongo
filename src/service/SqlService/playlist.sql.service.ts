
import {Playlist, Playlist_Song} from "../../interfaces/interface.types.js";
import userRepositorySQL from "../../db/sql/UserRepositorySQL.js";
import playlistRepositorySQL from "../../db/sql/PlaylistRepositorySQL.js";

class PlaylistServiceSQL {
    createPlaylist = async (body: Playlist) => {
        return await playlistRepositorySQL.create_playlist(body);
    };

    getPlaylistById = async (id: string) => {
        return await  playlistRepositorySQL.get_playlist_by_id(id);
        
    };

    getAllPlaylists = async () => {
        return await playlistRepositorySQL.get_all_playlists();   
    };

    deletePlaylistById = async (id: string) => {
        return await playlistRepositorySQL.delete_playlist_by_id(id);
        // delete from playlist_Song table
    };

    deleteAllPlaylists = async () => {
        return await playlistRepositorySQL.delete_all_playlists();

    };

    updatePlaylistById = async (id: string, body: Playlist) => {
        return await playlistRepositorySQL.update_playlist_by_id(id,body);
    };

    addSongToPlaylist = async (playlistid : string , songid : string) =>{
        return await playlistRepositorySQL.add_song_to_playlist(playlistid,songid);
    }

}

const artistServiceSQL = new PlaylistServiceSQL();
export default artistServiceSQL;
