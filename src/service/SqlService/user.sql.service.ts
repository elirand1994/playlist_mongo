
import {  User } from "../../interfaces/interface.types.js";
import userRepositorySQL from "../../db/sql/UserRepositorySQL.js";
class UserServiceSql {
    createUser = async (body: User) => {
        return await userRepositorySQL.create_user(body);
    };

    getUsertById = async (id: string) => {
        return await  userRepositorySQL.get_user_by_id(id);
        
    };

    getAllUsers = async () => {
        return await userRepositorySQL.get_all_users();   
    };

    deleteUserById = async (id: string) => {
       return await userRepositorySQL.delete_user_by_id(id);
    };

    updateUserById = async (id: string, body: User) => {
        return await userRepositorySQL.update_user_by_id(id,body);
    };

    login = async (email : string, password : string) =>{
        return await userRepositorySQL.login_user(email,password);
    }

}

const userServiceSQL = new UserServiceSql();
export default userServiceSQL;
