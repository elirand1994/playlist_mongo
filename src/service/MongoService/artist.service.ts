
import {  Artist } from "../../interfaces/interface.types.js";
import artistServiceMongo from "../../db/mongodb/ArtistMongoDB.js";
import artistsSQL from "../../db/sql/ArtistRepositorySQL.js";
class ArtistService {
    createArtist = async (body: Artist) => {
        //const response = await artistServiceMongo.createArtist(body);
        const response = await artistsSQL.create_artist(body);
        return response;
    };

    getArtistById = async (id: string) => {
        const response = await artistServiceMongo.getArtistById(id);
        return response;
    };

    getAllArtists = async () => {
        //const response = await artistServiceMongo.getAllArtists();
        const response = await artistsSQL.get_all_artists();
        return response;
    };

    deleteArtistById = async (id: string) => {
        const response = await artistServiceMongo.deleteArtistById(id);
        return response;
    };

    deleteAllArtists = async () => {
        const response = await artistServiceMongo.deleteAllArtists();
        return response;
    };

    updateArtistById = async (id: string, body: Artist) => {
        const response = await artistServiceMongo.updateArtistById(id,body);
        return response;
    };

    getAllSongsFromArtist = async (id: string) => {
        const response = await artistServiceMongo.getAllSongsFromArtist(id);
        return response;
}
}

const artistService = new ArtistService();
export default artistService;
