import {
    Playlist
} from "../../interfaces/interface.types.js";
import playlistServiceMongo from "../../db/mongodb/PlaylistMongoDB.js";
class PlaylistService {
    getAllPlaylists = async () => {
        const response = await playlistServiceMongo.getAllPlaylists();
        return response;
    };

    getPlaylistById = async (id: string) => {
        const response = await playlistServiceMongo.getPlaylistById(id);
        return response;
    };

    createPlaylist = async (body: Playlist) => {
        const response = await playlistServiceMongo.createPlaylist(body);
        return response;
    };

    deletePlaylistById = async (id: string) => {
        const response = await playlistServiceMongo.deletePlaylistById(id);
        return response;
    };

    deleteAllPlaylists = async () => {
        const response = await playlistServiceMongo.deleteAllPlaylists();
        return response;
    };

    addSongToPlaylistById = async (id: string, songid: string) => {
        const response = await playlistServiceMongo.addSongToPlaylistById(id,songid);
        return response;
    };

    getAllSongsFromPlaylistById = async (id: string) => {
        const response = await playlistServiceMongo.getAllSongsFromPlaylistById(id);
        return response;
    };

    getAllArtistsFromPlaylistById = async (id: string) => {
        const response = await playlistServiceMongo.getAllArtistsFromPlaylistById(id);
        return response;
    };

    deleteSongFromPlaylistById = async (id: string, songid: string) => {
        const response = await playlistServiceMongo.deleteSongFromPlaylistById(id,songid);
        return response;
    }
}

const playlistService = new PlaylistService();
export default playlistService;
