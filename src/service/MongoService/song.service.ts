
import { Song } from "../../interfaces/interface.types.js";
import songServiceMongo from "../../db/mongodb/SongMongoDB.js";
class SongService {
    addSong = async (body: Song) => {
        const response = await songServiceMongo.addSong(body);
        return response;
    };

    getSongById = async (id: string) => {
        const response = await songServiceMongo.getSongById(id);
        return response;
    };

    getAllSongs = async () => {
        const response = await songServiceMongo.getAllSongs();
        return response;
    };

    updateSongById = async (id: string, body: Song) => {
        const response = await songServiceMongo.updateSongById(id,body);
        return response;
    };

    deleteSongById = async (id: string) => {
        const response = await songServiceMongo.deleteSongById(id);
        return response;
    };

    deleteAllSongs = async () => {
        const response = await songServiceMongo.deleteAllSongs();
        return response;
    }
}

const songService = new SongService();
export default songService;
