import userMongoService from "../../db/mongodb/UserMongoDB.js";
class UserService {
    createUserService = async (body: any) => {
        const response = await userMongoService.createUserMongoService(body);
        return response;
    };

    loginService = async(body : any) =>{
        const response = await userMongoService.loginMongoService(body);
        return response;
    }

    logoutService = async (id : string) =>{
        const response = await userMongoService.logoutMongoService(id);
        return response;
    };
    paginateService = async (page: number, to_limit: number) => {
        const response = await userMongoService.paginateMongoService(page,to_limit);
        return response;
    };

    getAllUsersSerivce = async () => {
        const response = await userMongoService.getAllUsersMongoSerivce();
        return response;
    };

    getUserByIdService = async (id: string) => {
        const response = await userMongoService.getUserByIdMongoService(id);
        return response;
    };

    deleteUserByIdService = async (id: string) => {
        const response = await userMongoService.deleteUserByIdMongoService(id);
        return response;
    };
    updateUserByIdService = async (id: string, body: any) => {
        const response = await userMongoService.updateUserByIdMongoService(id,body);
        return response;
    };

}

const userService = new UserService();
export default userService;
